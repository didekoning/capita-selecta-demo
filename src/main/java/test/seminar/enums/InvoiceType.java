package test.seminar.enums;

enum InvoiceType {
    STANDARD,
    COMMERCIAL,
    PROGRESS
}