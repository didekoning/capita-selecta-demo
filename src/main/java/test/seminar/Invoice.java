package test.seminar;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Invoice {

    private int invoiceId;
    private String company;
    private List<InvoiceLine> standardInvoiceLines;
    private List<InvoiceLine> commercialInvoiceLines;
    private List<InvoiceLine> progressInvoiceLines;
    private static Logger logger = LoggerFactory.getLogger(Invoice.class);

    public Invoice(int invoiceId, String company) {
        this.invoiceId = invoiceId;
        this.company = company;
        standardInvoiceLines = new ArrayList<>();
        commercialInvoiceLines = new ArrayList<>();
        progressInvoiceLines = new ArrayList<>();
    }

    public int getAmountOfLines() {
        return standardInvoiceLines.size() + commercialInvoiceLines.size() + progressInvoiceLines.size();
    }

    //todo: check for new type of invoice
    public void addInvoiceLine(InvoiceLine line) {
        //try {
            if(line.getType() == 'S')
                standardInvoiceLines.add(line);
            if(line.getType() == 'C')
                commercialInvoiceLines.add(line);
            if(line.getType() == 'P')
                progressInvoiceLines.add(line);
        //}
        //catch (Exception e) {
            //System.out.println(e.getMessage());
            //logger.error(e.getMessage());
        //}
    }

    public int getInvoiceId() {
        return this.invoiceId;
    }

    public String getCompanyName() {
        return this.company;
    }
}