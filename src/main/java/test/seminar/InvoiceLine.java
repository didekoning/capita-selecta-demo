package test.seminar;

public class InvoiceLine {

    private char type;

    public InvoiceLine(char type) {
        this.type = type;
    }

    public char getType() {
        return type;
    }
}
