package test.seminar;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InvoiceTest {

    private Invoice invoice;
    private InvoiceLine line;

    @Before
    public void setUp() {
        invoice = new Invoice(1, "Hogeschool Utrecht");
    }

    @Ignore
    @Test
    public void addInvoiceLine() {
        line = new InvoiceLine('Z');
        invoice.addInvoiceLine(line);
        assertEquals(1, invoice.getAmountOfLines());
        assertEquals(1, invoice.getInvoiceId());
        assertEquals("Hogeschool Utrecht", invoice.getCompanyName());
    }

    @Ignore
    @Test
    public void addInvoiceLineAsNull() {
        line = null;
        invoice.addInvoiceLine(line);
        assertEquals(0, invoice.getAmountOfLines());
        assertEquals(1, invoice.getInvoiceId());
        assertEquals("Hogeschool Utrecht", invoice.getCompanyName());
    }
}